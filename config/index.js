export default {
  tailwindConfigPath: "./tailwind.config.js",
  style: {
    backgroundColor: "#151515",
    textColor: "#fff50a",
    width: "5rem",
    height: "5rem",
  },
};

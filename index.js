import path from "path";
import resolveConfig from "tailwindcss/resolveConfig";
import defaultConfig from "./config";

const { readdirSync } = require("fs");

export default function breakPointHelper(moduleOptions) {
  const moduleNamespace = "breakpointHelper";
  const mergeStyles = {
    ...defaultConfig.style,
    ...moduleOptions.style,
    ...(this.options.breakpointHelper
      ? this.options.breakpointHelper.style
      : {}),
  };

  const options = {
    ...defaultConfig,
    ...moduleOptions,
    ...this.options.breakpointHelper,
  };

  options.style = mergeStyles;

  if (this.options.dev) {
    // load tailwind config object - if user didn't pass a path of the config file, then search in the root of the project (where is nuxt.config.js)
    const tailwindConfig = require(path.join(
      this.options.srcDir,
      options.tailwindConfigPath
    ));

    // Add BreakpointHelper component plugin
    this.addPlugin({
      src: path.resolve(__dirname, "./plugins/index.js"),
      fileName: path.join(moduleNamespace, "plugins/index.js"),
      options: {
        tailwindConfig: resolveConfig(tailwindConfig),
        styleConfig: options.style,
      },
    });

    // sync all of the files and folders to revelant places in the nuxt build dir (.nuxt/)
    const foldersToSync = ["plugins", "components", "config"];
    for (const pathString of foldersToSync) {
      const pathToFolder = path.resolve(__dirname, pathString);
      for (const file of readdirSync(pathToFolder)) {
        this.addTemplate({
          src: path.resolve(pathToFolder, file),
          fileName: path.join(moduleNamespace, pathString, file),
        });
      }
    }
  }
}

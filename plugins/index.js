import Vue from 'vue'
import BreakpointHelper from '../components/BreakpointHelper.vue'

// get options object from module in runtime
const options = <%= JSON.stringify(options, null, 2) %>

const ExtendedBreakpointHelper = Vue.extend(BreakpointHelper)

// pass user's config from nuxt.config to the component
const breakpointHelper = new ExtendedBreakpointHelper({
  data() {
    return {
      customConfig: {
        tailwindConfig: options.tailwindConfig,
        styleConfig: options.styleConfig
      },
    }
  }
})

Vue.mixin({
  mounted() {
    // run this hook only on $root element to append BreakpointHelper component
    if (!this.$parent) {
      breakpointHelper.$mount()
      this.$root.$el.append(breakpointHelper.$el)
    }
  }
})





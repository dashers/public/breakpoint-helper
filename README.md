# NuxtJS + Tailwind CSS Breakpoint Helper

![npm (scoped)](https://img.shields.io/npm/v/@dashers/nuxt-tailwind-breakpoint-helper?style=flat-square)
![npm](https://img.shields.io/npm/dw/@dashers/nuxt-tailwind-breakpoint-helper?style=flat-square)
![NPM](https://img.shields.io/npm/l/@dashers/nuxt-tailwind-breakpoint-helper?style=flat-square)

NuxtJS module which shows current screen size based on your TailwindCSS config file in **development mode**.

<img src="https://gitlab.com/dashers/public/breakpoint-helper/-/raw/master/public/images/logo.png" width="300" height="300" >

## Example

![Example GIF](https://gitlab.com/dashers/public/breakpoint-helper/-/raw/master/public/images/example2.gif)

Demo in [Codesandbox](https://codesandbox.io/s/example-nuxtjs-tailwindcss-breakpoint-helper-jjhwl)

## Features

- Automatically mounts to your app
- Automatically loads your `tailwind.config.js` file
- You can pass another path to config file
- Customize colors and size

## Setup

Install with Yarn or NPM

```bash
# yarn
yarn add -D @dashers/nuxt-tailwind-breakpoint-helper

# npm
npm i @dashers/nuxt-tailwind-breakpoint-helper --save-dev
```

## Basic usage

You need to add `@dashers/nuxt-tailwind-breakpoint-helper` to your Nuxt config.

```javascript
// nuxt.config.js
{
  buildModules: [
    [
      "@dashers/nuxt-tailwind-breakpoint-helper",
      {
        // options
        style: {
          backgroundColor: "green",
          textColor: "#fff",
        },
      },
    ],
  ],
}
```

Or through separate options object

```javascript
// nuxt.config.js
{
  buildModules: ['@dashers/nuxt-tailwind-breakpoint-helper'],
  breakpointHelper: {
    style: {
      backgroundColor: 'green',
      textColor: '#fff',
    },
  },
}
```

Then it will automatically loads your Tailwind config file and mounts helper to your Nuxt app.

### Options

```javascript

// this is default options
{
  tailwindConfigPath: "./tailwind.config.js", // relative path from nuxt.config.js file
  // css3 values
  style: {
    backgroundColor: "#151515", // background-color
    textColor: "#fff50a", // color
    width: "5rem",
    height: "5rem",
  },
}
```

## Todo

- [ ] Minimize / hide breakpoint helper
- [ ] Draggable breakpoint helper
- [ ] Keyboard shortcuts

## Issues, questions & requests

If you have any questions or issues, please create an issue or a merge request in our [Gitlab repository](https://gitlab.com/dashers/public/breakpoint-helper).

## License

[MIT License](https://gitlab.com/dashers/public/breakpoint-helper/-/raw/master/LICENSE) - Copyright (c) Dasher
